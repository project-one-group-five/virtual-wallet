# _**This repo has moved to GitHub, for the updated repo visit - https://github.com/NGK02/sparki-virtual-wallet**_

<br>

Sparki Virtual Wallet
====================

>Sparki is a virtual wallet web app that lets you take charge of your budget! You can send and receive money, deposit cash from your credit or debit card, and even exchange currencies with real-time rates from a 3rd party API. Sparki lets users to effortlessly monitor their exchanges, transactions and transfers. It also shows them detailed financial data to help them make better decisions.

>The project is developed on the ASP.NET Core 6 framework with a graphic UI built with MVC and TailWind CSS entirely from scratch - no templates/themes used. We've also integrated some public API's which provide great added functionality.

## Project Description
### Key Features:
* **Email verification** - Secure your virtual wallet with email verification. It’s quick, easy, and adds an extra layer of protection to your account.
* **Currency exchange** - Sparki supports multiple currencies in your wallet. You can instantly exchange between your currencies with real dynamic exchange rates from a 3rd party API!
* **Detailed Transfer Records** - Options to sort your transactions based on your preferences, allowing you to quickly locate specific details and streamline your financial analysis.
* **Multiple cards** - With us you can easily manage all your debit and credit cards in one convenient place. Simply select the one you want to use within the app and you’re good to go.
* **Responsive design** - You can use our website on any device! We support multiple screen sizes from monitors, through tablets down to the smallest of smartphones!
* **Dark theme** - The dark theme is perfect for those who prefer a more subdued and elegant look and it's easier on the eyes. Plus, it can help save battery life on your device.
* **Refer a friend** - A fantastic opportunity for you to invite a friend and both of you enjoy a delightful gift.

#### Home Page
* The home page of our virtual wallet showcases real-time active user count, total transactions count, our main features and options to Login, Register, Refer a friend and more. 

![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/HomePageDark.png)

#### Login Page
* The login page provides visitors with the opportunity to access the full potential of the website by logging in with their accounts, unlocking various features and functionalities.

![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/Login.PNG)

### Register Page
* Visitors can get some info on the website’s features and register to unlock the app’s full functionality.

![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/Register.PNG)

### Logged User Options
* Dashboard - Opens the Main Dashboard page.
* View Profile - Opens detailed profile page.
* Sign Out - Doesn't need description.

![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/DropDown.PNG)

### View profile
* On this page every user can see and edit their profile information.

![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/Profile.PNG)

### Main Dashboard
* Within this page, each user has an advanced overview of their financial landscape. Here users can review their balances, see details of their credit and debit cards, and a dynamic diagram illustrating their transactions from the past seven days.

    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MainDashboard.PNG)

### Make Transfer
* Every user can deposit into their wallet directly from their debit or credit card. Additionally, users have the freedom to initiate withdrawals.
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MakeTransfer.PNG)

    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MakeTransferForm.PNG)

* Readonly confirmation Page where you can confirm your transfer or edit it.

    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MakeTransferConfirm.PNG)

* Message for successful transfer.
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/STransfer.PNG)

    
### Make Exchange
* Every user has the opportunity to exchange their currencies at the latest real-time rates. 
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MakeExchange.PNG)

    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MakeExchangeForm.PNG)

* Readonly confirmation page where you can check the rate and the expected amount and confirm your exchange or edit it.

    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MakeExchangeConfirm.PNG)

* Message for Successful Exchange.
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/SExchange.PNG)

    ### Make Transaction
* With just a few clicks, users can effortlessly send or receive funds from other users. 
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MakeTransaction.PNG)

* Users can find their recipient by Username, Email and Phone number. 

    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MakeTransactionForm.PNG)

* Readonly confirmation page where you can confirm your transaction or edit it (Only the Username of the recipient is shown).

    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/MakeTransactionConfirm.PNG)

* Message for successful Transaction.
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/STransaction.PNG)

## View Exchanges
* Every user has the ability to view all their exchanges with options to Order them by Descending/Ascending and Sort them by:
    * Date
    * From Currency
    * To Currency
    * Amount
    * Exchanged Amount

    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/ViewExchanges.PNG)

## View Transactions
* Every user has the ability to view all their transactions with options to Order them by Descending/Ascending and Search or Sort them by:
* Filter By:
    * Recipient username
    * Sender username
    * From Date
    * To Date
* Sort By:
    * Date
    * Currency
    * Amount
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/ViewTransactions.PNG)

    ## View Transfers
* Every user has the ability to view all their transfers with options to Order them by Descending/Ascending and Sort them by:
    * Date
    * From Currency
    * To Currency
    * Amount
    * Exchanged Amount
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/ViewTransfers.PNG)
    
## Administrative part
* When an admin is logged into the system, they gain access to the Admin panel. Within the Admin panel, admins are able to search for users based on their username, email address or phone number.
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/DropDownAdmin.PNG)
    <br><br>
* From the Admin panel, administrators have access to all user profiles, allowing them to view detailed user information and manage user accounts effectively, including the ability to block or unblock accounts , view their profiles (by clicking on their names) and full history of their transactions (by clicking on username).
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/AdminPanel.PNG)
    <br><br>
* If you try to access the any other user's data without being an admin. You will be greeted by our awesome custom error page!
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/ErrorPageDark.gif)
    <br><br>
## Responsive design
* Here are some screencaps from out app on mobile to show it is fully responsive!
    <br><br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/Responsive.png)
    <br><br>

## API
* Sparki Wallet provides a REST API documentation in Swagger, allowing developers to easily explore and understand the available endpoints, request/response structures, and supported operations.
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/API.PNG)

## Database Diagram
* A detailed database diagram is included to provide a an idea for the underlying structure of Sparki Wallet. This diagram visually represents the relationships between different database tables and the structure of the data model.
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/DataBaseDiagram.PNG)

## Installation
* Download project from the repository.
* Connect Database and seed the stored data inside. You can do this by installing the NuGet package Microsoft.EntityFrameworkCore for VS and running the following commands:
```
add-migration initial
update-database
```
* Run the project 
* Register 
* Use all functionalities :)

## Additionally
* Exchange Rate API - https://www.exchangerate-api.com
* Email service - https://sendgrid.com
* Auto-Mapper
* Data transfer objects(DTOs)
* Public API's
* In-memory database, MSTest and Moq for testing
* Unit test code coverage of the business logic
* Technologies
     * ASP.NET Core
     * Entity Framework Core
     * Mock Framework
     * MSSQL Server
     * TailWind CSS
     * HTML
     * CSS
## Team Members
* Atanas Iliev - [GitLab](https://gitlab.com/atanasiliev1293)
* Nikolai Gigov - [GitLab](https://gitlab.com/NG02)
* Katrin Lilova - [GitLab](https://gitlab.com/katrinlilova)
* Telerik Acedemy Official Project
<br>
    ![Alt text](https://gitlab.com/project-one-group-five/virtual-wallet/-/raw/dev/ImagesForREADME/telerik.PNG)
